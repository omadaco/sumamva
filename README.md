# Servicio Dockerizado
> Servicio REST que realiza la suma de dos numeros y que se ejecuta sobre la plataforma Docker.

[![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]

El propósito de esta tarea es poder poner en práctica los conceptos de Docker a través de la realización de un servicio que se ejecute en esta plataforma. Para esto se requiere como parte de la tarea lo siguiente:

	1. Crear un servicio que retorne la suma de 2 números (El servicio puede ser implementado en cualquier lenguaje de programación. Debe ser SOAP o REST). El servicio debe ser desplegado en una imagen Docker para su consumo.
	2. Publicar el código en GitHub/GitLab
	3. Agregar como parte del código la imagen docker (Dockerfile)
	4. Publicar la imagen Docker en Docker Hub
	5. Agregar como parte del código los scripts que permitan crear/destruir la imagen docker de manera local. Estos archivos deben ser llamados start.sh y stop.sh
	6. Agregar la documentación para consuimir el servicio en GitHub (README)
	7. Utilizar las etiquetas (tags) UJaveriana, AES, ModVal en las descripciones de GitHub/GitLab y DockerHub


![](../header.png)

## Instalación

Windows:

Abrir la consola para ejecutar el siguiente comando y asi descargar el repositorio de codigo del ejercicio:


```sh
git clone https://gitlab.com/omadaco/sumamva.git
```

## Ejemplo de uso

1. Abrir la consola e ingresar al directorio donde fue descargado el repositorio. Ejemplo:

```sh
cd  sumamva
```

2. Parar ejecutar el bash "start", ejecutar el comando 

```sh
sh start.sh
```

3. Una vez finalice la ejecucion del bash start.sh abrir un navegador web e ingresar en la barra de direcciones la siguiente URL:

[http://localhost/sumamva/index.php/sumas/4/1](http://localhost/sumamva/index.php/sumas/4/1)

Con esto se podra consumir el servicio REST en donde los dos parametros numericos en la URL seran sumandos y se obtendra el resultado en un JSON.

4. Para detener todo el despliegue la imagen Docker se debe ejecutar en la consola el siguiente comando:

```sh
sh stop.sh
```

## Historial de versiones

* 0.0.1
    * Publicacion del codigo del servicio REST

## Meta

Omar David Cotes Hernandez – [@omadaco](https://twitter.com/omadaco) – omadaco@gmail.com

Distribuido bajo la licencia XYZ. Ver ``LICENSE`` para más información.

[https://gitlab.com/omadaco/sumamva.git](https://gitlab.com/omadaco/sumamva.git)

[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
