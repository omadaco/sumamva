<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$app = new \Slim\App();

$app->get('/sumas/{valor}/{numero}', function (Request $request, Response $response) {
    $valor = $request->getAttribute('valor');
    $numero = $request->getAttribute('numero');
    $suma=intval($valor)+intval($numero);
    $data = array('resultado' => $suma);
    $newResponse = $response->withHeader('Content-type', 'application/json');
    $newResponse = $response->withJson($data);
  
    return $newResponse;
  
});

$app->run();
